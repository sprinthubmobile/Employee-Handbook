---
layout: post
title: Management Team
---

# Management Team

### CEO
Sokari Gillis-Harry
Twitter: @cybersokari
LinkedIn: https://www.linkedin.com/in/sokari/
Email: sokari@sprinthub.com.ng

### Board

Private and visible to internal team.

#### Contact

If you would like to contact the management team. Please send an email to management@sprinthub.com.ng
