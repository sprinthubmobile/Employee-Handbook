---
layout: post
title: Creating new projects
---

# Creating new projects

There will be few times where you'll be required to create new project. Use boiler plate code to create a new project as they have a lot of best practices built in. 

- **ReactJS**: Use this template: https://github.com/react-boilerplate/react-boilerplate
- **Android**: Check out [Android Jetpack](https://developer.android.com/jetpack/)

