---
layout: post
title: Onboarding for Engineers
---

# Onboarding for Engineers

- [Read this first]({{ site.baseurl }}{% link people-operations/onboarding/README.md %})
- Sign up on GitHub, Bitbucket (if you don't have an account already) and send your username to your manager and he'll add you to the organisation.
- Join #engineering channel on Slack.
- We don't usually assign email accounts to interns, If you're a Junior Engineers or Sr Engineer - request one by emailing legal@sprinthub.com.ng

- [Working with Trello]({{ site.baseurl }}{% link engineering/trello/README.md %})
- [Code Review and Practices]({{ site.baseurl }}{% link engineering/code-review/README.md %})
- [Working with clients]({{ site.baseurl }}{% link engineering/consulting/clients/README.md %})
