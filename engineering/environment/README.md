---
layout: post
title: Local Development Environment for WEB DEVELOPMENT
---

# Local Development Environment for WEB DEVELOPMENT

- Use an Ubuntu machine. That's the best flavor of linux out there. We embrace open source and run open source in production, so its best to have your dev environment identical to production. If you're using Windows - switch to Ubuntu. MacOS is occasionally okay since its POSIX. We DON'T recommend using one because its overpriced and gives you exactly the same functionality an Ubuntu machine would give you with third the cost. 
- We use Visual Studio Code as our primary IDE. If you're using anything else, we HIGHLY recommend you to switch to Visual Studio Code. 
- IMPORTANT: Learning about debuging in VS Code is important. It'll save you days of your life (trust us!). [Please check this short 8 min video out](https://www.youtube.com/watch?v=2oFKNL7vYV8)
- IMPORTANT: [Please check this short video on Debugging in Chrome](https://www.youtube.com/watch?v=H0XScE08hy8). It's worth watching. 
- Use Robo Mongo for Local MongoDB Data Viewing and Editing. Download it from here [Robo 3T](https://robomongo.org/download). Important: Do not download Studio 3T but Robo 3T. If you're using Compass on your local machine. Switch to Robo Mongo as it's a much better tool.
- Use Chrome as primary browser. It makes devs lives simpler. 
- [Add React Dev Tools to Chrome](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)
- [Add ColorZilla to Chrome](https://chrome.google.com/webstore/detail/colorzilla/bhlhnicpbhignbdhedgjhgdocnmhomnp?hl=en)
- If you need icons, svg, png's for your project - [FlatIcon](https://www.flaticon.com/) is the best reesource out there. 
- If you're looking for color matching , color combinations. Please check [Color Wheel by Adobe](https://color.adobe.com/)
- If you're looking for design inspiration. Please check [Dribbble](https://dribbble.com/)
- If you're looking for project templates. Please check [WrapBootstrap](https://wrapbootstrap.com/)


# Local Development Environment for MOBILE APP DEVELOPMENT

- Use a MacOS. That's the best tool for mobile development out there. If you're using Windows - switch to MacOS. Ubuntu is occasionally okay since its POSIX. We DON'T recommend using one because it currently do not support native iOS developement. 
- We use Andriod Studio and Xcode as our primary IDE for mobile app development. If you're using anything else, we HIGHLY recommend you to switch to Andriod Studio for Andriod development and Xcode for iOS development. 
- IMPORTANT(Android): Learning about debuging in Android Studio is important. It'll save you days of your life (trust us!). [Please check this short read](https://developer.android.com/studio/debug/)
- IMPORTANT(iOS): Learning about debuging in Xcode is also important. It'll save you days of your life (trust us!) [Please check this Udacity free course on Debugging in Xcode](https://www.udacity.com/course/xcode-debugging--ud774). Also check the official documentation on [Xcode Debugging](https://developer.apple.com/library/archive/documentation/DeveloperTools/Conceptual/debugging_with_xcode/chapters/debugging_tools.html) It's worth watching and reading. 
- Use Chrome as primary browser. It makes devs lives simpler. 
- [Add ColorZilla to Chrome](https://chrome.google.com/webstore/detail/colorzilla/bhlhnicpbhignbdhedgjhgdocnmhomnp?hl=en)
- If you need icons, svg, png's for your project - [FlatIcon](https://www.flaticon.com/) is the best reesource out there. 
- If you're looking for color matching , color combinations. Please check [Color Wheel by Adobe](https://color.adobe.com/)
- If you're looking for design inspiration. Please check [Dribbble](https://dribbble.com/)
