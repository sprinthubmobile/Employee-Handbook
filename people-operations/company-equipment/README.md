---
layout: post
title: Company Equipment
---

# Company Equipment

- We want to make sure you're productive all the time, and we want to equip you with best machines possible.
- If you need a machine. We'll try to provide one for you. This will be an  Ubuntu machine or Macbook with a decent Processor and RAM. We DONT provide Windows machines at this point.
- This will be a Company laptop and In the event of you leaving the company this should be returned to Company HQ in Port Harcourt, Nigeria or to any other Employee who might need one.
- To Request one: [Please sign here.](https://docs.google.com/document/d/1jAtUIEB-sJDtt-EYz3-LJ1Y-NI8CjIhcl36u3OHFC9M/edit?usp=sharing)
- You're ONLY eligible for this if you've worked more than 3 months with us and currently work 40 hours weekly.
- We also do RAM upgrades as well. If you want to request RAM, Please sign the same document.
- If a machine is available from past employees that we consider is good, we will courier that to you instead and not buy a new one.
- If you fail to return the equipment after you leave the company. We might see this as theft of company's property, and we'll file a report with local police.
- Only Employees in Nigerian are eligible for this. We're working on to have this in other countries soon.

