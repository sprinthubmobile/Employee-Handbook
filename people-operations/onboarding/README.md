---
layout: post
title: Employee Onboarding
---

# Employee Onboarding

This document is for all new employees.

- Please fill this [form](https://docs.google.com/forms/d/e/1FAIpQLSeT_5Sd109HkyIF8L7SJ_4wG3S3eQnv7ot14B5Ers3F4Rsizg/viewform) and give us your interview feedback. 


- If you haven't signed your offer letter already. Its a good idea to do it right now before proceeding further. If your offer letter is not counter-signed by someone in the SprintHub team. Let the person who interviewed you know immediately.

- If you don't know who you report to, Ask someone you interviewed you immediately.

- When you're on Slack:
  - Please make sure your username is <firstname><lastname> and you have a good profile picture uploaded to Slack. This will ensure team members can recognize you and contact you easily.
  - We use Slack as our primary mode of communication. It's a good idea to have the Slack app downloaded and installed both on your Desktop and on your phone.
  - Please also update your Slack Profile with Email, Phone Number, and Skype Username. It'll be easier for other team members to reach out.
  - Our company Slack URL is sprinthub.slack.com

- Send your Passport (if you don't have your passport, send in your valid Govt ID(Ex. National ID or Voters Card) instead. Also, apply for a passport immediately.), Your Address, Your Bank Account Details (Ac No,TIN, Account Name, Bank Name, Branch Name, and Bank Address), Your National ID, Your valid Tax ID, Your primary and secondary contact number - to legal@sprinthub.com.ng

- Sign up on Trello (if you don't have an account already) and send your username to your manager.

##Remote Devs
- Sign up to [Hubstaff](https://hubstaff.com) if you work remote. Select a FREE plan, create a new organisation / project called 'sprinthub'. Send your username and password (yes, we need your password here). Send the credentials to legal@sprinthub.com.ng. Since this is a remote job, like any other remote company we use Hubstaff to track time.



- [Values]({{ site.baseurl }}{% link general/values/README.md %})
- [General Guidelines]({{ site.baseurl }}{% link general/guidelines/README.md %})
- [Basic Etiquette]({{ site.baseurl }}{% link general/basic-etiquette/README.md %})
- [Objective and Key Results (OKR)]({{ site.baseurl }}{% link OKR/README.md %})
  - [Company OKR]({{ site.baseurl }}{% link OKR/company/README.md %})
- [Learning Resources]({{ site.baseurl }}{% link learn/README.md %})
- [Working with Hubstaff]({{ site.baseurl }}{% link people-operations/hubstaff/README.md %})
- [Salary and Leave Policy]({{ site.baseurl }}{% link people-operations/salary-and-leave/README.md %})
- [Salary Slabs]({{ site.baseurl }}{% link people-operations/salary/README.md %})
- [Standups]({{ site.baseurl }}{% link people-operations/standups/README.md %})
- [Salary and Leave Policy]({{ site.baseurl }}{% link people-operations/salary-and-leave/README.md %})
- [Salary Slabs]({{ site.baseurl }}{% link people-operations/salary/README.md %})
- [Performance Review]({{ site.baseurl }}{% link people-operations/performance-review/README.md %})
- [Underperformace]({{ site.baseurl }}{% link people-operations/underperformance/README.md %})
- [Visa]({{ site.baseurl }}{% link people-operations/visa/README.md %})
- [Company Equipment]({{ site.baseurl }}{% link people-operations/company-equipment/README.md %})
- [Healthcare]({{ site.baseurl }}{% link people-operations/healthcare/README.md %})
- [Quarterly Feedback]({{ site.baseurl }}{% link people-operations/feedback/README.md %})
- [Violation]({{ site.baseurl }}{% link people-operations/violation/README.md %})
- [Resignation]({{ site.baseurl }}{% link people-operations/resignation/README.md %})
- [Life at SprintHub]()
- [For Engineers]({{ site.baseurl }}{% link engineering/onboarding/README.md %})
- [For Admin]({{ site.baseurl }}{% link admin/onboarding/README.md %})
- [For Sales]({{ site.baseurl }}{% link sales/onboarding/README.md %})
- [For Marketing]({{ site.baseurl }}{% link marketing/onboarding/README.md %})

- Important: If you leave the company before 12 months. We will not provide you with an experience and reliveing letter. Your employment with us will be invalid and you will no longer be able to refer us to your future employers. 

If you have any questions. Please contact your manager.
