---
layout: post
title: Writing Content
---

# Writing Content

Most of the task we do is using the latest technologies, best practises and design principles (Java, Javascript, Swift, Android, iOS, AdobeXD, SCRUM etc) to build solutions(Mobile Apps, SDKs, Web Apps and UI/UX designs) which help businesses and companies acheive their goals faster. It's important we have developers and designers talk about the work we do and sell the solutions we build instead of sales people. One of the ways we do it is by [Inbound Marketing](https://www.hubspot.com/inbound-marketing) which includes writing blogs. 

We require everyone in the company to write one blog / month. This is not optional. We know some of you may not be good at writing, but we will help you with it every step of the way. Time you spend to write blogs is paid for by the company. You can do this whenever you like. You can write a blog on any one of our products (even if you're not the one working on it). There's a lot of room for you to explore.

Before you start writing, please discuss the topic and title of your blog with your manager. Ask your manager to add you on [Medium](https://medium.com) as a writer. Once you're done - submit the blog to a `publication` and we'll have it reviewed and give you feedback, just like we give you feedback on your code.

You can also switch writing blogs to making a Video Course, Webinars, or whatever makes you tick. 

**Important** If you're working on Blogs. [Here's an important guide from our friend at Hackerbay](https://blog.hackerbay.io/checklist-for-publishing-a-story-to-medium-aaf811b68526) that you can read. Do not submit your blogs to Medium if you have not completed these steps. 




