---
layout: post
title: Healthcare
---

# Healthcare

- This option is ONLY for Full Time SprintHub Employees
- This is only for you and your immediate family (your spouse and kids, if you have any)
- We cant have it for your parents at this point. You would have to buy separate insurance for them.
- If you have immediate family (spouse + kids). Let us know when you’re enrolling for an insurance.
- To enroll, Ping @cybersokari

### Nigeria

We'll subscribe you at [NHIS.gov.ng](https://www.nhis.gov.ng/)


### Other Countries

- Unfortunately, we don't support health insurance in other countries yet. Please talk to local insurance company to get you and your family covered. We **highly recommend** you do. 
