---
layout: post
title: Standups and Meetings
---

# Standups and Meetings

- Standups happen on Zoom. Install both Desktop and Mobile App before the standup.
- Make sure you're in the meeting 5 mins before the scheduled time, so you can troubleshoot problems as they arise.
- Link to the standup is pinned on #general channel. All company wide meetings and standups happen here.

### Company-wide Daily Standups

- We bring the entire company together every Monday-Friday 9:30 PM - 10:00 PM West Africa time (GMT +1)
- This is mandatory to attend.  If you miss these, you lose a half of your salary for the day.
- We bring the entire company together to make sure everyone is on the right page. People from Sales, Marketing, Admin, Engineering will be here and you will know updates from everyone in the team.


### C-Level Standup (Weekly)

- They happen on Mon (9:00-9:30 PM) West Africa time (GMT +1).
- C-level Execs are required to attend. Its mandatory for them.
- These standups are also open to the entire company. So, If you want to join - feel free, but its optional for everyone else. Feel free to also share your feedback, and bounce ideas.

### Company Wide (Monthly)

- They happens on the last friday of every month (9:00-10:00 PM ) West Africa time (GMT +1).
- Everyone is required to attend these.
- If 28th is Monday, then C-Level Standup happen on the same call.
- If you miss these [a violation]({{ site.baseurl }}{% link people-operations/violation/README.md %}) is registered against you.
- This is essential for monthly feedback, strategy discussions, progress report, and more. 
