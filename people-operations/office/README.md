---
layout: post
title: Working at our fffice
---


Our Office space very vital to the kind work we do. As a company we will always strive to make comfortable work spaces available for those of us that are not working remotely. If you will be working from one of our offices we expect you to adhere to the common rules of modern work spaces. Below are our office etiquette.


#  Open-Office Etiquette

1. Respect another’s need to work.
Just because others are sitting nearby doesn’t mean they are available for conversation at all times. Respect one another’s privacy. Act as if there is a door between you and if they appear to be busy, ask if they have a moment to talk.

2. Be aware of smells.
Within a tight space smells can be magnified, so use consideration when packing your lunch or snacks. Try to eat meals in the kitchen, break room or outside, rather than at your desk. Since many people have allergies to scents, forgo wearing perfumes, cologne or strong after shave to the office. Pay attention to your personal grooming as well. Unfortunately, common sense is not so common anymore.

3. Noise. 
Success in our new environment necessitates consideration and cooperation relative to noise that distracts.
Here are some pointers to maintain concentration in a collaboratively focused setting.
 + Modulate your voice. No need to whisper – just be considerate of your neighbors when speaking near individual
workspaces.
 + Use phone discretion. Speakerphone use, lengthy conversations and interactive webinars should only happen in
enclosed spaces. If you are using a cellphone, removing one earbud can help you keep your voice at an appropriate
level.
 + Be conscious of non-verbal noise. Keep cell phone and other devices on vibrate or on a low ringtone. If you have
a large quantity of copies to be made or shredding to be done, consider running during times when there are fewer
people in the surrounding area.
 + Take advantage of huddle and gathering spaces. Move noisy conversations away from individual work settings.
Lounge and café spaces are designed for impromptu, small group meetings.
 + Reserve enclosed spaces when needed. Team activity spaces are available for larger or longer meetings.
Enclave and refuge rooms are provided for extended conference calls, one-on-one conversations and focus work
time.

4. Be tidy.
Your messy desk can be a distraction to others and will detract from the professional image your organization is trying to establish. Keep your belongings confined to your own personal space and tidy up your immediate area each day before leaving work. If you share a desk, be sure to clear away any personal items like coffee cups and office supplies.

5. Respect another’s space.
Just because another’s workspace is within reach of your desk doesn’t make it common domain. Treat each person’s space as if it was a private office. Do not help yourself to anything on their desk or in their area. Ask first or go to the supply closet if you need a pen or a stapler.

6. Interruptions. 
Successful co-existence in an open office setting relies on clear communication and mutual
respect.
 + Signify privacy preference. When appropriate, use `headphones` or other methods to manage distractions and to
indicate that you are engaged in focus work.
 + Read and respect privacy cues and personal boundaries. If your neighbor is wearing `headphones` or otherwise
signaling they are doing focused work, hold your thought for another time or send them a message

7. Don’t come to work sick.
When you work in close quarters, it is easy to transfer germs. Stay home if you are sick. It’s good hygiene to cover your mouth when you cough, keep hand sanitizer on hand, don’t leave used tissues around, and wipe down the desk, computer keyboard and phone from time to time to help prevent germs from spreading.

8. Be considerate.
Respect is key when working in an open-office environment. Act respectful and expect others to act in the same way. Set rules of conduct and reiterate boundaries when they are crossed. It’s best to address problems and concerns directly and diplomatically before they escalate.

8. Be tolerant.
The open-office environment brings together myriad personalities, with different styles. Be tolerant of these differences and find ways to adapt. Everyone is not going to agree with you one hundred percent of the time. Keep an open mind, listen with the intent to learn and focus on the positive aspects of your job.

9. Think like a team.
In order to maintain a cohesive team, do not spread gossip, cause another to feel like an outcast, or grumble about petty things. Hold regular meetings to set goals, share ideas and talk about concerns.

## Headquater
Find the address to HQ on [Google Map](https://goo.gl/maps/ihxH7taYtmr).