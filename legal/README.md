---
layout: post
title: Legal
---

# Legal

### Corporate

Sprint Innovation Hub Ltd. is a Nigerian Company. Our registered address in the Nigeria is Pavilion Building, Happy Rollings Street, Off East-West Road, Alakahia, Obio Akpor 500102, Port Harcourt.

Employer Identification Number for Sprint Innovation Hub Ltd is RC 1508469

### Contracts
If you need to request legal resources, including contract review, third party license review, legal advice or guidance, please submit all requests through email at legal@sprinthub.com.ng

Please be sure to include sufficient detail regarding your request, including time-sensitive deadlines, relevant documents, and background information necessary to respond.

### Chat Channel
Feel free to use the `#legal` chat channel in Slack for general legal questions.

## Company Policies

* [Anti-harassment]({{ site.baseurl }}{% link general/anti-harassment/README.md %})
