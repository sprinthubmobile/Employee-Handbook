---
layout: post
title: 2FA and Password Managers
---

##2FA and Password Managers

>>**tl;dr - use 2FA everywhere you can.  use a password manager like lastpass/1password etc., too.**

The Internet’s a scary place, and we keep most of our cool stuff there. We need to do everything we can to keep the baddies out. That way we can sleep better at night, without the teeth grinding and nail biting and other anxious habits that might emerge worrying that baddies are accessing our stuff.


##Google Account
**Instructions on enabling 2FA for Google Accounts can be found at:**
https://support.google.com/accounts/answer/180744?hl=en

A day or so after your start date, 2FA enforcement is enabled, requiring 2FA in order for you to access your google account.

##Trello
**Instructions on enabling 2FA for Trello can be found at:**
https://trello.com/2fa

##GitLab
**Instructions on enabling 2FA for GitLab can be found at:**
https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html

##Github
**Instructions on enabling 2FA for Github can be found at:**
https://help.github.com/articles/configuring-two-factor-authentication-via-a-totp-mobile-app/


**Note:** Github ain’t messing around. If you lose your 2FA access method, it’s very unlikely that they will reset your account and let you back in. Be sure to print out those backup codes! Seriously. On paper. Don’t store them on your phone.

*If you've lost access to your account after enabling two-factor authentication, GitHub can't help you gain access again. Having access to your recovery codes in a secure place, or establishing a [secondary mobile phone number for recovery](https://help.github.com/articles/setting-a-fallback-authentication-number/), will get you back into your account.*

##Slack
**Instructions on enabling 2FA for Slack can be found at:**
https://slack.zendesk.com/hc/en-us/articles/204509068-Enabling-two-factor-authentication


##All the other things

Any company related service that you have access to which supports 2FA should have it enabled. 

##All the non-2FA things

For services that do not support some form of 2FA, it’s triply important that you have a strong unique password (12+ characters, non-dictionary, etc.) that’s not shared with any other accounts. In order to comply with this requirement and avoid a life of sticky notes covered with crazy passwords, we are asking everyone to use a Password Manager such as [1Password](https://agilebits.com/onepassword) or [KeePass](http://keepass.info/). Please take some time today to set one of these up and add all of your work related accounts to it (be sure to de-duplicate any passwords that you use in more than one place and take advantage of the strong password generation tools that Password Managers offer to increase the length of any weak ones).

##Oh and one OTHER thing - unsolicited password reset requests

If you ever get a password reset request that you don’t know about/didn’t request, **let someone on the systems team know**.  Don’t assume it’s benign, and let us check it out to make sure everything’s OK.

***
If you have any questions about a specific account or 2FA in general, please reach out to IT.