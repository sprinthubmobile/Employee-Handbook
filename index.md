---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# SprintHub Employee Handbook


The SprintHub Employee Handbook is the central repository for how we run the company. As part of our value of being transparent the handbook is open to the world, and we welcome feedback. Please make a pull request to suggest improvements or add clarifications. Few sections of this handbook may not open to the world and is only accessible to employees.

Please use issues to ask questions.

* General
  * [Mission Statement]({{ site.baseurl }}{% link general/vision-mission/README.md %})
  * [Values]({{ site.baseurl }}{% link general/values/README.md %})
  * [General Guidelines]({{ site.baseurl }}{% link general/guidelines/README.md %})
  * [Basic Etiquette]({{ site.baseurl }}{% link general/basic-etiquette/README.md %})
  * [Work Ethic]({{ site.baseurl }}{% link general/work-ethic/README.md %})
  * [Anti-Harassment Policy]({{ site.baseurl }}{% link general/anti-harassment/README.md %})
  * [Paycheck]({{ site.baseurl }}{% link general/paycheck/README.md %})
* [Objective and Key Results (OKR)]({{ site.baseurl }}{% link OKR/README.md %})
  * [Company OKR]({{ site.baseurl }}{% link OKR/company/README.md %})
* [Learning Resources]({{ site.baseurl }}{% link learn/README.md %})
* People Operations(HR)
  * [Onboarding]({{ site.baseurl }}{% link people-operations/onboarding/README.md %})
  * [Standups]({{ site.baseurl }}{% link people-operations/standups/README.md %})
  * [Working with Hubstaff]({{ site.baseurl }}{% link people-operations/hubstaff/README.md %})
  * [Salary and Leave Policy]({{ site.baseurl }}{% link people-operations/salary-and-leave/README.md %})
  * [Salary Slabs]({{ site.baseurl }}{% link people-operations/salary/README.md %})
  * [Performance Review]({{ site.baseurl }}{% link people-operations/performance-review/README.md %})
  * [Underperformance]({{ site.baseurl }}{% link people-operations/underperformance/README.md %})
  * [Visa]({{ site.baseurl }}{% link people-operations/visa/README.md %})
  * [Company Equipment]({{ site.baseurl }}{% link people-operations/company-equipment/README.md %})
  * [Open-Office Etiquettes]({{ site.baseurl }}{% link people-operations/office/README.md %})
  * [Healthcare]({{ site.baseurl }}{% link people-operations/healthcare/README.md %})
  * [Quarterly Feedback]({{ site.baseurl }}{% link people-operations/feedback/README.md %})
  * [Violation]({{ site.baseurl }}{% link people-operations/violation/README.md %})
  * [Resignation]({{ site.baseurl }}{% link people-operations/resignation/README.md %})
  * [Writing Blogs]({{ site.baseurl }}{% link people-operations/blogs/README.md %})
* Engineering
  * [FAQ for Interview Applicants]({{ site.baseurl }}{% link engineering/applicants/faq/README.md %})
  * [Onboarding for Engineers]({{ site.baseurl }}{% link engineering/onboarding/README.md %})
  * [Development Environment]({{ site.baseurl }}{% link engineering/environment/README.md %})
  * [Working with Trello]({{ site.baseurl }}{% link engineering/trello/README.md %})
  * [Code Review and Practices]({{ site.baseurl }}{% link engineering/code-review/README.md %})
  * [Performance Review for Engineers]({{ site.baseurl }}{% link engineering/performance-review/README.md %})
  * [Markdown Guide](https://guides.github.com/features/mastering-markdown/)
  * [Working with clients]({{ site.baseurl }}{% link engineering/consulting/clients/README.md %})
  * [Interviewing Software Engineers]({{ site.baseurl }}{% link engineering/interviewing/README.md %})
  * [Creating new projects]({{ site.baseurl }}{% link engineering/new-project/README.md %})
* Admin
  * [Onboarding for Admin]({{ site.baseurl }}{% link admin/onboarding/README.md %})
* Marketing
  * [Onboarding for Marketing]({{ site.baseurl }}{% link marketing/onboarding/README.md %})
* Sales
  * [Onboarding for Sales]({{ site.baseurl }}{% link sales/onboarding/README.md %})
* Finance
  * [Board Meetings]({{ site.baseurl }}{% link finance/board-meetings/README.md %})
  * [Stock Options]({{ site.baseurl }}{% link finance/stock-options/README.md %})
* Leadership
  * [Board of Advisors](https://sprinthub.slack.com/files/U033XTX4D/F5AGZ5W7J/Board_of_Advisors)
  * [Management Team]({{ site.baseurl }}{% link leadership/management/README.md %})
* [Legal]({{ site.baseurl }}{% link legal/README.md %})
  * For Customers and Clients
    * [Non-Disclosure Agreement](https://docs.google.com/document/d/179pyJJyThlisFeBKpaxtuj77vG9MhTVjiWvixqMAQ_8/edit?usp=sharing)
    * [Software Development Contract](https://docs.google.com/document/d/1hBJntoH1yuuk4N6Z5mFV7EWeiI89aESgErEAu-0Dn0w/edit?usp=sharing)
  * For Employees
    * [Employment Offer Letter](https://docs.google.com/document/d/1tKA8bwDJ19v11u5lgIyEhiIPRnLsHZnPpQbfeL318YI/edit?usp=sharing)
    * [Contractor Agreement](https://docs.google.com/document/d/1hq8p3Oyjoup9cjkR2GsXeiCiwwh6dQ3E-2WSsO7drhI/edit?usp=sharing)
    * [Promotion Letter](https://docs.google.com/document/d/1TDm0MpbiKdiJRdZqZnEjDyDTrX0BZ7sDPm9wjyFHvWc/edit?usp=sharing)
    * [Company Property Agreement](https://docs.google.com/document/d/1jAtUIEB-sJDtt-EYz3-LJ1Y-NI8CjIhcl36u3OHFC9M/edit?usp=sharing)
    * [Relieving Letter](https://docs.google.com/document/d/1pF1OcLSpG-Cx8A16owY0K5Xav0PHiimP3DNgFSU-AJE/edit?usp=sharing)
* [Security]({{ site.baseurl }}{% link security/README.md %})
* [Credits]({{ site.baseurl }}{% link credits/README.md %})