---
layout: post
title: Sales Onboarding
---

# Sales Onboarding

- Join #sales channel on Slack
- Request for Zoho CRM access from your manager. We use that as our CRM.
- Request an email account by emailing legal@sprinthub.com.ng
